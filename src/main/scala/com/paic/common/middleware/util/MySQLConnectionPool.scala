package com.paic.common.middleware.util

import java.sql.Connection

import com.mchange.v2.c3p0.ComboPooledDataSource
import org.apache.commons.logging.LogFactory

class MySQLConnectionPool extends Serializable {
  val logger = LogFactory.getLog(this.getClass);
  private val cpds: ComboPooledDataSource = new ComboPooledDataSource(true)
  private val properties = MySQLPropertyUtils.getMySqlPoolConfigProperties();
  try {
    cpds.setJdbcUrl(properties.getProperty("jdbcURL"))
    cpds.setDriverClass(properties.getProperty("driverClass"));
    cpds.setUser(properties.getProperty("username"));
    cpds.setPassword(properties.getProperty("password"))
    cpds.setMaxPoolSize(Integer.parseInt(properties.getProperty("maxPoolSize")))
    cpds.setMinPoolSize(Integer.parseInt(properties.getProperty("minPoolSize")))
    cpds.setAcquireIncrement(Integer.parseInt(properties.getProperty("acquireIncrement")))
    cpds.setMaxStatements(Integer.parseInt(properties.getProperty("maxStatements")))
  } catch {
    case e: Exception => e.printStackTrace()
  }

  def getConnection: Connection = {
    try {
      return cpds.getConnection();
    } catch {
      case ex: Exception =>
        ex.printStackTrace()
        null
    }
  }
}