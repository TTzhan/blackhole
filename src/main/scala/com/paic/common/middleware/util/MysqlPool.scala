package com.paic.common.middleware.util

import java.sql.Connection

import com.mchange.v2.c3p0.ComboPooledDataSource
import com.paic.common.util.ReadPropertyUtils

class MysqlPool extends Serializable {
  private val cpds: ComboPooledDataSource = new ComboPooledDataSource(true)
  try {
    cpds.setJdbcUrl(ReadPropertyUtils.getFileProperties("mysql-user.properties", "mysql.jdbc.url"));
    cpds.setDriverClass(ReadPropertyUtils.getFileProperties("mysql-user.properties", "mysql.pool.jdbc.driverClass"));
    cpds.setUser(ReadPropertyUtils.getFileProperties("mysql-user.properties", "mysql.jdbc.username"));
    cpds.setPassword(ReadPropertyUtils.getFileProperties("mysql-user.properties", "mysql.jdbc.password"))
    cpds.setMinPoolSize(40)
    cpds.setMaxPoolSize(200)
    cpds.setAcquireIncrement(5)
    cpds.setMaxStatements(180)
  } catch {
    case e: Exception => e.printStackTrace()
  }
  def getConnection: Connection = {
    try {
      return cpds.getConnection();
    } catch {
      case ex: Exception =>
        ex.printStackTrace()
        null
    }
  }
  
  def close() = {
    try {
      cpds.close()
    } catch {
      case ex: Exception =>
        ex.printStackTrace()      
    }
  }
}
object MysqlManager {
  var mysqlManager: MysqlPool = _
  def getMysqlManager: MysqlPool = {
    synchronized {
      if (mysqlManager == null) {
        mysqlManager = new MysqlPool
      }
    }
    mysqlManager
  }
}