package com.paic.common.middleware.util

/**
  * Created by Jerry on 2018/5/9.
  *
  */
object MySQLPoolConnectionManager {
  var mysqlConnections: MySQLConnectionPool = _
  def getMysqlConnection: MySQLConnectionPool = {
    synchronized {
      if (mysqlConnections == null) {
        mysqlConnections = new MySQLConnectionPool
      }
    }
    mysqlConnections
  }
}
