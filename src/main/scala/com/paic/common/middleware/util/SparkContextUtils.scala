package com.paic.common.middleware.util

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SQLContext

object SparkContextUtils {
  def initSparkSQLContext(sparkMasterURL: String, taskName: String): SQLContext = {
    val sparkContext = initSparkContext(sparkMasterURL, taskName)
    val sqlContext = new org.apache.spark.sql.SQLContext(sparkContext)
    sqlContext
  }
  def initSparkContext(sparkMasterURL: String, taskName: String): SparkContext = {
    val sparkConf = new SparkConf()
    sparkConf.setAppName(taskName)
    sparkConf.setMaster(sparkMasterURL)
    sparkConf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    //sparkConf.set("spark.kryoserializer.buffer.mb", "512")
    sparkConf.set("spark.kryo.referenceTracking", "false")
    val sparkContext = new SparkContext(sparkConf)
    sparkContext
  }
}