package com.paic.common.util;

import java.io.File;
import java.nio.ByteBuffer;

/**
 *将原IP.java中，load接口涉及到的所有变量封装到一起，作为load接口的返回值进行返回，
 *从而可以将load的结果通过广播变量的方式在spark中进行广播，并在后期传入find接口，对load的结果进行查询，提升操作效率。
 * 
 * @author liaoxiaoyi
 *
 */
public class IPLoadResult{
	private  int offset;
	private  int[] index = new int[256];
	private  ByteBuffer dataBuffer;
	private  ByteBuffer indexBuffer;
	private  Long lastModifyTime = 0L;
	private  File ipFile;
	
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int[] getIndex() {
		return index;
	}
	public void setIndex(int[] index) {
		this.index = index;
	}
	public ByteBuffer getDataBuffer() {
		return dataBuffer;
	}
	public void setDataBuffer(ByteBuffer dataBuffer) {
		this.dataBuffer = dataBuffer;
	}
	public ByteBuffer getIndexBuffer() {
		return indexBuffer;
	}
	public void setIndexBuffer(ByteBuffer indexBuffer) {
		this.indexBuffer = indexBuffer;
	}
	public Long getLastModifyTime() {
		return lastModifyTime;
	}
	public void setLastModifyTime(Long lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}
	public File getIpFile() {
		return ipFile;
	}
	public void setIpFile(File ipFile) {
		this.ipFile = ipFile;
	}
	
	
}
