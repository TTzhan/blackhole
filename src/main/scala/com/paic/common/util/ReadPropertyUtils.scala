package com.paic.common.util

import java.util.concurrent.ConcurrentHashMap
import scala.io.Source
import com.typesafe.config.ConfigFactory
import java.util.Properties

object ReadPropertyUtils {
  def getFileProperties(fileName: String, propertyKey: String): String = {
    val result = this.getClass.getClassLoader.getResourceAsStream(fileName)
    val prop = new Properties
    prop.load(result)
    prop.getProperty(propertyKey)
  }
}